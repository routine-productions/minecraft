$(document).ready(function () {

    $('.JS-Tabs').each(function () {
        var Hash = $(this).find('.JS-Tabs-Navigation .JS-Tab.Active').attr('data-href');
        $(this).find('.JS-Tabs-Content [data-tab=' + Hash + ']').show().siblings('[data-tab !=' + Hash + ']').hide();

    });

    $('.JS-Tabs .JS-Tabs-Navigation .JS-Tab').click(function () {

        var Hash = $(this).attr('data-href');
        $(this).parents('.JS-Tabs-Navigation').find('.JS-Tab').removeClass('Active');

        $(this).parents('.JS-Tabs').find('.JS-Tabs-Content [data-tab=' + Hash + ']').show().siblings('[data-tab !=' + Hash + ']').hide();
        $(this).addClass('Active');

        if (Hash) {
            history.pushState(null, null, "#" + Hash);
        }
    });

    $('.Restore-Password').click(function () {
        
    });
});



