$('.Filter-Button').click(function () {

    if ($('.Filter-Menu').is(':hidden')) {
        $('.Filter-Menu').slideUp();
        $('.Filter-Open').addClass('DisActive');
        $('.Filter-Close').addClass('Active');
        $('.Header-Filters').addClass('Active');
    } else {
        $('.Filter-Menu').slideDown();
        $('.Filter-Open').removeClass('DisActive');
        $('.Filter-Close').removeClass('Active');
        $('.Header-Filters').removeClass('Active');
    }

    return false;
});


$('.Filter-List li').click(function () {

    if ($(this).hasClass('Active')) {
        $(this).removeClass('Active');
    } else {
        $(this).addClass('Active');
    }

    return false;
});


$('.Card-Liker').each(function (index, elem) {

    var Count;

    $(elem).find('.Like-Down').click(function () {

        Count = parseInt($(elem).find('.Like-Count').text());

        $(elem).find('.Like-Count').text(Count - 1);

    });

    $(elem).find('.Like-Up').click(function () {

        Count = parseInt($(elem).find('.Like-Count').text());

        $(elem).find('.Like-Count').text(Count + 1);

    });

});


$('.JS-Scroll-Top').click(function () {

    if ($(window).scrollTop() > 0) {
        var Scroll_Duration_String = $(this).attr('data-scroll-duration') ? $(this).attr('data-scroll-duration') : 400,
            Scroll_Duration = parseInt(Scroll_Duration_String);

        $('body, html').animate({
            scrollTop: 0
        }, Scroll_Duration);
    }

    return false;
});


$('.Header-Hamburger').click(function () {

    if ($(window).width() <= 1024) {
        if ($(this).hasClass('is-active')) {
            $(this).removeClass('is-active');
            $('.Header-Menu nav').removeClass('Active');

        } else {
            $(this).addClass('is-active');
            $('.Header-Menu nav').addClass('Active');
        }
    }

    return false;
});


