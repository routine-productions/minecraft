<section class="Main-Content">
    <div class="Wrapper">
        <ul class="Article-List">
            <?php for($l = 0; $l < 4; $l++) { ?>
                <li class="Card-Item">
                    <?php require "./modules/card.php" ?>
                </li>
            <?php } ?>
        </ul>
    </div>
    <?php require "./modules/pagenav.php" ?>


    <div class="Button-Up JS-Scroll-Top">
        <svg><use xlink:href="#arrow"></use></svg>
    </div>
</section>