<section class="Footer">
    <div class="Wrapper">
        <div class="Footer-Content">
            <div class="Footer-Block">
                <h4 class="Footer-Title">Навигация</h4>
                <ul class="Footer-List">
                    <li><a href="">FAQ</a></li>
                    <li><a href="">Реклама и контакты</a></li>
                    <li><a href="">Блог и новости</a></li>
                    <li><a href="">Вход / регистрация</a></li>
                </ul>
            </div>
            <div class="Footer-Block">
                <h4 class="Footer-Title">Сервера</h4>
                <ul class="Footer-List">
                    <li><a href="">Рейтинг серверов</a></li>
                    <li><a href="">Новые сервера</a></li>
                    <li><a href="">Добавить сервер</a></li>
                    <li><a href="">Раскрутить сервер</a></li>
                </ul>
            </div>
            <div class="Footer-Block">
                <h4 class="Footer-Title">Последнее в блоге</h4>
                <ul class="Footer-List">
                    <li><a href="">Мы запустили свой лаунчер - MRLauncher</a></li>
                    <li><a href="">MinecraftRating.ru оптимизирован под мобильные устройства</a></li>
                    <li><a href="">Начиная с 1 Мая голоса за сервера будут обнуляться каждый месяц!</a></li>
                    <li><a href="">Топ серверов</a></li>
                    <li><a href="">Добавлена возможность скрыть IP адрес сервера</a></li>
                </ul>
            </div>
            <div class="Footer-Block">
                <h4 class="Footer-Title">Мы в соцсетях</h4>
                <ul class="Footer-Socials">
                    <li>
                        <a href="">
                            <span>
                                <svg class="Vk-Logo">
                                    <use xlink:href="#vk-logo"></use>
                                </svg>
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <span>
                                <svg class="Fb-Logo">
                                    <use xlink:href="#fb-logo"></use>
                                </svg>
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <span>
                                <svg class="Ok-Logo">
                                    <use xlink:href="#ok-logo"></use>
                                </svg>
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <span>
                                <svg class="Tw-Logo">
                                    <use xlink:href="#tw-logo"></use>
                                </svg>
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <span>
                                 <svg class="Gp-Logo">
                                     <use xlink:href="#gp-logo"></use>
                                 </svg>
                            </span>
                        </a>
                    </li>
                </ul>
                <h4 class="Footer-Title">Счетчики</h4>
                <ul class="Footer-Counter">
                    <li>
                        <img src="/img/counter-01.png" alt="">
                    </li>
                    <li>
                        <img src="/img/counter-02.gif" alt="">
                    </li>
                    <li>
                        <img src="/img/counter-03.gif" alt="">
                    </li>
                    <li>
                        <img src="/img/counter-04.gif" alt="">
                    </li>
                    <li>
                        <img src="/img/counter-05.png" alt="">
                    </li>
                    <li>
                        <img src="/img/counter-06.png" alt="">
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="Footer-Copyright">
        <div class="Wrapper">
            <span>&copy; Minecraft-Servers.ru, 2016 г.</span>
        </div>
    </div>
</section>





