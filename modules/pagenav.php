<nav class="Pagenav">
    <span class="Pagenav-Beginning">
        <a href="">
            <svg>
                <use xlink:href="#arrow-double"></use>
            </svg>
        </a>
    </span>
    <span class="Step-Prev">
        <a href="">
            <svg>
                <use xlink:href="#arrow"></use>
            </svg>
        </a>
    </span>
    <ul>
        <?php for ($k = 0; $k < 15; $k++) { ?>
            <li class="Active">
                <a href="">
                    <?= $k ?>
                </a>
            </li>
        <?php } ?>
    </ul>
    <span class="Step-Next">
        <a href="">
            <svg>
                <use xlink:href="#arrow"></use>
            </svg>
        </a>
    </span>
     <span class="Pagenav-End">
         <a href="">
             <svg>
                 <use xlink:href="#arrow-double"></use>
             </svg>
         </a>
    </span>
</nav>