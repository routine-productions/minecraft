<section class="Header">
    <div class="Wrapper">
        <div class="Header-Menu">
            <div class="Header-Logo">
                <a href="/">
                    <img src="" alt="">

                    <h1>Minecraft<span>Rating</span></h1>
                </a>
            </div>
            <nav>
                <ul>
                    <li class="Active">
                        <a href="">
                            новые сервера
                        </a>
                    </li>
                    <li>
                        <a href="">
                            топ серверов
                        </a>
                    </li>
                    <li>
                        <a href="">
                            новости
                        </a>
                    </li>
                    <li>
                        <a href="">
                            реклама и контакты
                        </a>
                    </li>
                    <li class="JS-Modal-Button" href="#Modal-1">
                        войти
                    </li>
                </ul>
            </nav>
            <button class="Header-Hamburger hamburger--slider hamburger" type="button">
              <span class="hamburger-box">
                <span class="hamburger-inner"></span>
              </span>
            </button>
        </div>
    </div>
    <div class="Header-Filters">
        <div class="Filters-Nav">
            <div class="Wrapper">
                <div class="Show-Filters">
                    <div class="Filter-Block Left">
                        <div class="Filter-Button">
                            <div class="Filter-Open">
                                <svg>
                                    <use xlink:href="#arrow-double"></use>
                                </svg>
                                <span>Показать дополнительные фильтры</span>
                            </div>
                            <div class="Filter-Close">
                                <svg>
                                    <use xlink:href="#delete"></use>
                                </svg>
                                <span>Скрыть дополнительные фильтры</span>
                            </div>
                        </div>
                    </div>
                    <div class="Filter-Block Right">
                        <div class="Header-Search">
                            <svg>
                                <use xlink:href="#magnifier"></use>
                            </svg>
                            <input type="text" placeholder="поиск по назвнию сервера">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="Filter-Menu">
            <div class="Wrapper">
                <div class="Filter-List">
                    <span class="Filter-Title">Версия сервера</span>
                    <ul>
                        <?php for ($i = 0; $i < 50; $i++) { ?>
                            <li>
                                <a href="">
                                    1.<?= $i ?>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="Filter-List">

                    <span class="Filter-Title">Мини игры</span>
                    <ul>
                        <?php for ($i = 0; $i < 5; $i++) { ?>
                            <li class="Active">
                                <a href="">
                                    Голодные игры
                                </a>
                            </li>
                        <?php } ?>
                    </ul>

                </div>
                <div class="Filter-List">

                    <span class="Filter-Title">Моды</span>
                    <ul>
                        <?php for ($i = 0; $i < 10; $i++) { ?>
                            <li>
                                <a href="">
                                    Оружие
                                </a>
                            </li>
                        <?php } ?>
                    </ul>

                </div>
                <div class="Filter-List">

                    <span class="Filter-Title">Плагины</span>
                    <ul>
                        <?php for ($i = 0; $i < 5; $i++) { ?>
                            <li>
                                <a href="">
                                    Hypixelpets
                                </a>
                            </li>
                        <?php } ?>
                    </ul>

                </div>
                <div class="Filter-List">

                    <span class="Filter-Title">Основные</span>
                    <ul>
                        <?php for ($i = 0; $i < 10; $i++) { ?>
                            <li>
                                <a href="">
                                    Выживание
                                </a>
                            </li>
                        <?php } ?>
                    </ul>

                </div>
            </div>
        </div>

    </div>
</section>
