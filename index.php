<!DOCTYPE html>
<html>

<head lang="ru">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Minecraft</title>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,300,500italic,700&subset=latin,cyrillic'
          rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/index.min.css" type="text/css"/>
    <link rel="icon" type="image/png" href="/img/">
</head>

<body>
<?php require_once __DIR__ . "/img/sprite.svg"; ?>

<header>
    <?php require_once __DIR__ . "/modules/header.php"; ?>
</header>

<main>
    <?php
    if ($_SERVER['REQUEST_URI'] == '/') {
        require_once __DIR__ . '/pages/main.php';
        require_once __DIR__ . '/pages/card_full.php';
    } else {
        require_once __DIR__ . '/pages/' . $_SERVER['REQUEST_URI'] . '.php';
    }
    ?>
</main>

<footer>
    <?php require_once __DIR__ . "/modules/footer.php"; ?>
</footer>


<?php require_once __DIR__ . "/modules/modal-cabinet.php"; ?>

<script src="/index.min.js"></script>
<body>
</html>